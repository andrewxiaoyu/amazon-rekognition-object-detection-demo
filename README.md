# Amazon Rekognition Object Detection Demo

This is a basic demo on using Amazon Rekognition in pure JavaScript.
It includes:
-   how to detect labels on an uploaded image client-side
-   how to draw bounding boxes on the image output